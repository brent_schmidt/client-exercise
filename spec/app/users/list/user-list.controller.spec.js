'use strict';

describe('user-list.controller', function main() {

  let $controller, $rootScope;
  let mockUserService = {};
  let ctrl, indexStub;

  const mockUsers = [
    {
      id: 1,
      name: 'User XYZ',
      username: 'user1',
      email: 'user1@test.com'
    },
    {
      id: 2,
      name: 'User ABC',
      username: 'user2',
      email: 'user2@test.com'
    }
  ];

  beforeEach(angular.mock.module('ce'));
  beforeEach(angular.mock.module(($provide) => {
    $provide.value('userService', mockUserService);
  }));

  beforeEach(inject((_$controller_, _$rootScope_) => {
    indexStub = sinon.stub().resolves(mockUsers);
    mockUserService.index = indexStub;

    $controller = _$controller_;
    $rootScope = _$rootScope_;

    ctrl = $controller('UserListController', { $scope: $rootScope.$new() });
  }));

  it('should call the user service index method', (done) => {
    ctrl.$onInit()
      .then(() => {
        expect(indexStub.called).to.be.true;
        done();
      })
      .catch((err) => done(err));
  });

  it('should sort the users', (done) => {
    ctrl.$onInit()
      .then(() => {
        expect(ctrl.users[0].name).to.equal('User ABC');
        done();
      })
      .catch((err) => done(err));
  });

});
