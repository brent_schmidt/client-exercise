'use strict';

describe('user-details.controller', function main() {

  let $componentController, $rootScope;
  let mockUserService = {};
  let ctrl, queryStub;

  const mockUsers = [
    {
      id: 1,
      name: 'User XYZ',
      username: 'user1',
      email: 'user1@test.com'
    },
    {
      id: 2,
      name: 'User ABC',
      username: 'user2',
      email: 'user2@test.com'
    }
  ];

  beforeEach(angular.mock.module('ce'));
  beforeEach(angular.mock.module(($provide) => {
    $provide.value('userService', mockUserService);
  }));

  beforeEach(inject((_$componentController_, _$rootScope_) => {
    queryStub = sinon.stub().resolves(mockUsers);
    mockUserService.query = queryStub;

    $componentController = _$componentController_;
    $rootScope = _$rootScope_;

    ctrl = $componentController('userDetails', { $scope: $rootScope.$new() }, { userId: 1 });
  }));

  it('should call the user service query method with the user id.', (done) => {
    ctrl.$onInit()
      .then(() => {
        expect(queryStub.calledWith(1)).to.be.true;
        done();
      })
      .catch((err) => done(err));
  });

});
