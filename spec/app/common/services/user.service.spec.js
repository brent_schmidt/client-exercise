'use strict';

describe('user.service', function main() {

  beforeEach(angular.mock.module('ce'));

  let userService;
  let userServiceConstants;

  const mockUsers = [
    {
      id: 1,
      name: 'User 1',
      username: 'user1',
      email: 'user1@test.com'
    },
    {
      id: 2,
      name: 'User 2',
      username: 'user2',
      email: 'user2@test.com'
    }
  ];

  beforeEach(inject((_userService_, _userServiceConstants_) => {
    userService = _userService_;
    userServiceConstants = _userServiceConstants_;
  }));

  it('should have an index method.', () => {
    expect(userService.index).to.be.defined;
  });

  it('should have a query method.', () => {
    expect(userService.query).to.be.defined;
  });

  describe('HTTP Calls', function main() {

    let $httpBackend;

    beforeEach(inject((_$httpBackend_) => {
      $httpBackend = _$httpBackend_;

      $httpBackend
        .when('GET', userServiceConstants.endpoint)
        .respond(mockUsers);

      $httpBackend
        .when('GET', `${userServiceConstants.endpoint}?id=1`)
        .respond([mockUsers[0]]);
    }));

    afterEach(() => {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should make the correct HTTP call when index is invoked', () => {
      $httpBackend.expectGET(userServiceConstants.endpoint);
      const call = userService.index();
      $httpBackend.flush();

      expect(call).to.eventually.become(mockUsers);
    });

    it('should make the correct HTTP call when query is invoked', () => {
      $httpBackend.expectGET(`${userServiceConstants.endpoint}?id=1`);
      const call = userService.query(1);
      $httpBackend.flush();

      expect(call).to.eventually.become([mockUsers[0]]);
    });
  });
});
