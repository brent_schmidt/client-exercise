// Setup our test globals

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');

// Extend chai with promise methods.
chai.use(chaiAsPromised);

// Extend sinon with promise methods.
// NOTE: This will soon not be necessary, as the promise extensions will be pulled
// into the main sinon package.
require('sinon-as-promised');

window.expect = chai.expect;
window.should = chai.should();
window.assert = chai.assert;
window.sinon = sinon;
