'use strict';

import fs from 'fs';

const files = fs.readdirSync('./gulp');
files.forEach((file) => {
  require(`${process.cwd()}/gulp/${file}`);
});
