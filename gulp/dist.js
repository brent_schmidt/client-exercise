import gulp from 'gulp';
import gutil from 'gulp-util';
import runSequence from 'run-sequence';

gulp.task('dist', (done) => {
  runSequence(
    'browserify:vendors',
    'browserify:app',
    'sass:app',
    'copy',
    (err) => {
      if (err) {
        done(new gutil.PluginError('task:disk', err));
      } else {
        done();
      }
    }
  );
});
