import gulp from 'gulp';
import gutil from 'gulp-util';
import karma from 'karma';

gulp.task('test', ['dist'], (done) => {
  const files = [
    'public/js/app.vendors.js',
    'public/js/app.module.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'spec/test-setup.js',
    'spec/**/*.spec.js'
  ];
  const options = {
    files,
    configFile: `${__dirname}/../karma.conf.js`
  };
  const server = new karma.Server(
    options,
    (code) => {
      if (code) {
        done(new gutil.PluginError('task:disk', `${gutil.colors.red('TEST ERROR:')} Code: ${code}`));
      } else {
        done();
      }
    }
  );
  server.start();
});
