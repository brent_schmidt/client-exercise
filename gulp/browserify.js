'use strict';

import browserify from 'browserify';
import buffer from 'vinyl-buffer';
import globby from 'globby';
import gulp from 'gulp';
import gutil from 'gulp-util';
import resolve from 'resolve';
import source from 'vinyl-source-stream';
import sourcemaps from 'gulp-sourcemaps';
import tsify from 'tsify';
import uglify from 'gulp-uglify';

const isProduction = process.env.ENVIRONMENT === 'production';

gulp.task('browserify:app', function (done) {
  globby(['src/**/*.ts'])
    .then((entries) => {
      const b = browserify({
        entries,
        paths: ['./src'],
        debug: !isProduction
      })
        .plugin(tsify)
        .transform('babelify', {
          extensions: ['.ts']
        });

      getVendorDependencies().forEach((id) => b.external(id));

      let err;
      b.bundle()
        .on('error', function errHandler(_err) {
          err = _err;
          this.emit('end');
        })
        .pipe(source('app.module.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: !isProduction }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/js'))
        .on('end', function () {
          done(err);
        });
    });
});

gulp.task('browserify:vendors', () => {
  const b = browserify({
    debug: !isProduction
  });

  getVendorDependencies().forEach((id) => b.require(resolve.sync(id), { expose: id }));

  return b
    .bundle()
    .on('error', function errHandler(err) {
      gutil.log(err.message);
      this.emit('end');
    })
    .pipe(source('app.vendors.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: !isProduction }))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public/js'));
});

function getVendorDependencies() {
  // read package.json and get dependencies' package ids
  let ids;
  try {
    const packageManifest = require(`${process.cwd()}/package.json`);
    ids = Object.keys(packageManifest.dependencies) || [];
  } catch (e) {
    gutil.log(`${gutil.colors.red('ERROR:')} ${e.message}`);
  }
  return ids;
}
