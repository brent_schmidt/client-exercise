'use strict';

import gulp from 'gulp';
import rename from 'gulp-rename';

gulp.task('copy', ['copy:html', 'copy:html:index', 'copy:assets']);

gulp.task('copy:html', () => {
  gulp.src(['src/**/*.html', '!src/app/app.html'])
    .pipe(gulp.dest('public/'));
});

gulp.task('copy:html:index', () => {
  gulp.src('src/app/app.html')
    .pipe(rename('index.html'))
    .pipe(gulp.dest('public/'));
});

gulp.task('copy:assets', () => {
  gulp.src('images/**/*')
    .pipe(gulp.dest('public/images'));
});
