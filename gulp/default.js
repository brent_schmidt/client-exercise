'use strict';

import browserSync from 'browser-sync';
import gulp from 'gulp';
import gutil from 'gulp-util';
import path from 'path';
import runSequence from 'run-sequence';

gulp.task('default', ['clean'], (done) => {
  const bs = browserSync.create();

  gulp.watch(['src/**/*.html'], { debounceDelay: 250 }, (event) => {
    let taskName;
    // Normalize the path to be platform agnostic
    const filePath = event.path.split(path.sep).join('/');
    if (filePath.includes('src/app/app.html')) {
      taskName = 'copy:html:index';
    } else {
      taskName = 'copy:html';
    }
    runSequence(
      taskName,
      (err) => {
        if (err) {
          gutil.log(err.message);
        } else {
          bs.reload();
        }
      }
    );
  });

  gulp.watch(['src/**/*.ts'], { debounceDelay: 250 })
    .on('change', (event) => {
      runSequence(
        'browserify:app',
        (err) => {
          if (err) {
            gutil.log(err.message);
          } else {
            bs.reload();
          }
        }
      );
    });

  gulp.watch(['src/**/*.scss'], { debounceDelay: 250 }, () => {
    runSequence(
      'sass:app',
      (err) => {
        if (!err) {
          bs.reload();
        }
      }
    );
  });

  runSequence(
    'dist',
    (err) => {
      if (!err) {
        bs.init({
          server: {
            baseDir: './public'
          }
        }, done);
      } else {
        process.exit(2);
      }
    }
  );
});
