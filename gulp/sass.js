'use strict';

import cleanCss from 'gulp-clean-css';
import gulp from 'gulp';
import gutil from 'gulp-util';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';

gulp.task('sass:app', (done) => {
  gulp.src('./src/app/app.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(
      {
        includePaths: ['node_modules/']
      }
    ).on(
      'error',
      (err) => done(new gutil.PluginError(`sass:app`, err.messageFormatted)))
    )
    .pipe(cleanCss())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public/styles/'))
    .on('end', () => done());
});
