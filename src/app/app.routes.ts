function AppRoutes($stateProvider: ng.ui.IStateProvider) {
  $stateProvider
    .state({
      name: 'index',
      url: '/',
      template: '<header><h1>Client Exercise</h1></header>'
    })
    .state('users', {
      url: '/users',
      abstract: true,
      template: '<ui-view></ui-view>',
      redirectTo: 'users.list'
    })
    .state('users.list', {
      url: '',
      template: '<user-list></user-list>'
    })
    .state('users.id', {
      url: '/{id}',
      template: (params: any): string => {
        return `<user-details user-id="${params.id}"></user-details>`;
      }
    });
}

AppRoutes.$inject = ['$stateProvider'];

angular
  .module('ce')
  .config(AppRoutes);
