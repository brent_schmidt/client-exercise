function runCeModule(
  $location: ng.ILocationService,
  $state: ng.ui.IStateService
) {
  const path = $location.path();
  if (!path || path === '/') {
    // Redirect to root/index state.
    $state.go('index');
  }
}
runCeModule.$inject = ['$location', '$state'];

angular
  .module('ce')
  .run(runCeModule);
