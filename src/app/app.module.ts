// Bootstrap our dependencies
import 'angular';
import 'angular-resource';
import 'angular-ui-router';

declare global {
  const angular: ng.IAngularStatic;
}

angular
  .module('ce', [
    'ngResource',
    'ui.router'
  ]);
