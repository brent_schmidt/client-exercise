interface IUserServiceConstants {
  endpoint: string;
}

angular
  .module('ce')
  .constant({
    userServiceConstants: {
      endpoint: 'http://jsonplaceholder.typicode.com/users'
    }
  });
