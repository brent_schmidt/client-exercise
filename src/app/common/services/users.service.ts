export interface IUser {
  id: number;
  name: string;
  username: string;
  email: string;
  address: {};
  [name: string]: any;
}

export interface IUserResource extends ng.resource.IResource<IUser> {
}

export interface IUserResourceClass extends ng.resource.IResourceClass<IUserResource> {
  index: ng.resource.IResourceArrayMethod<IUserResource>;
}

export interface IUserService {
  index(): Promise<ng.resource.IResourceArray<IUserResource>>;
  query(id: number): Promise<ng.resource.IResourceArray<IUserResource>>;
}

export class UserService implements IUserService {
  public static $inject = ['$resource', 'userServiceConstants'];

  private userResourceClass: IUserResourceClass;

  public constructor(
    private $resource: ng.resource.IResourceService,
    userServiceConstants: IUserServiceConstants
  ) {
    // Create the user resource.
    this.userResourceClass = <IUserResourceClass> $resource(
      userServiceConstants.endpoint,
      {},
      {
        query: { method: 'GET', params: { id: '' }, isArray: true },
        index: { method: 'GET', isArray: true }
      }
    );
  }
  public async index() {
    return await this.userResourceClass.index().$promise;
  }
  public async query(id: number) {
    return await this.userResourceClass.query({ id }).$promise;
  }
}

angular
  .module('ce')
  .service('userService', UserService);
