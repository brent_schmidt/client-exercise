function AddressComponent(): ng.IComponentOptions {
  const options: ng.IComponentOptions = {};

  options.templateUrl = './app/common/layout/address/address.html';
  options.bindings = {
    condensed: '<?',
    address: '<?'
  };

  return options;
}

angular
  .module('ce')
  .component('address', AddressComponent());
