import { IUserResource, IUserService } from 'app/common/services/users.service';

class UserDetailsController implements ng.IController {

  public static $inject = [
    '$log',
    '$scope',
    'userService'
  ];

  public user: IUserResource;
  // bindings
  private userId: number;

  constructor(
    private $log: ng.ILogService,
    private $scope: ng.IScope,
    private userService: IUserService
  ) { }

  public async $onInit() {
    if (!this.userId) {
      this.$log.error('No userId value provided for user component');
      return;
    }

    const users = await this.userService.query(this.userId);
    if (!users.length) {
      this.$log.error(`UserId [${this.userId}] not found.`);
    }

    this.user = users[0];
    // Trigger a digest, safely.
    this.$scope.$evalAsync();
  }
}

angular
  .module('ce')
  .controller('UserDetailsController', UserDetailsController);
