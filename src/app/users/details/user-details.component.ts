import { IUserResource } from 'app/common/services/users.service';

function UserDetailsComponent(): ng.IComponentOptions {
  const options: ng.IComponentOptions = {};
  options.templateUrl = './app/users/details/user-details.html';
  options.controller = 'UserDetailsController';
  options.bindings = {
    userId: '@'
  };
  return options;
}

angular
  .module('ce')
  .component('userDetails', UserDetailsComponent());
