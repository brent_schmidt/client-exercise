class UserListDirective implements ng.IDirective {
  public restrict = 'E';
  public templateUrl = './app/users/list/user-list.html';
  public controller = 'UserListController';
  public controllerAs = '$ctrl';
  public bindToController = true;

  public static factory(): ng.IDirectiveFactory {
    return () => new UserListDirective();
  }
}

angular
  .module('ce')
  .directive('userList', UserListDirective.factory());
