import { IUser, IUserResource, IUserService } from 'app/common/services/users.service';

class UserListController implements ng.IController {

  public static $inject = ['$scope', 'userService'];
  public users: IUserResource[] = [];
  public thing: string;

  constructor(
    private $scope: ng.IScope,
    private userService: IUserService
  ) { }

  public async $onInit() {
    await this.getUserList();
  }

  private async getUserList() {
    this.users = await this.userService.index();
    // Sort descending
    // FIXME: Find out how to pull in the real types 
    // of the array members, but still keep the TSC
    // happy about us trying to access IUser member
    // values.
    this.users.sort((a: any, b: any) => {
      const nameA = a.name.toUpperCase();
      const nameB = b.name.toUpperCase();

      if (nameA < nameB) {
        return 1;
      }
      if (nameA > nameB) {
        return -1;
      }
      return 0;
    });
    // Trigger a digest, safely.
    this.$scope.$evalAsync();
  }
}

angular
  .module('ce')
  .controller('UserListController', UserListController);
